pragma solidity >=0.4.2;

contract Login {
    event LoginAttempt(address indexed _sender, string _challenge);

    function login(string memory _challenge) public {
        emit LoginAttempt(msg.sender, _challenge);
    }
}