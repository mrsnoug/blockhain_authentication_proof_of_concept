from django.core.management import BaseCommand
from django_ethereum_events.models import Daemon, MonitoredEvent, FailedEventLog



class Command(BaseCommand):
    help = 'Cleans the app state; only for dev and debbuging usage\
        after its execution "print_events" function should print nothing'

    def handle(self, *args, **options):
        Daemon.get_solo().delete()
        self.stdout.write(self.style.SUCCESS('Reset block monitoring to 0...'))

        MonitoredEvent.objects.all().delete()
        FailedEventLog.objects.all().delete()

        self.stdout.write(self.style.SUCCESS('Deleted failed event logs...'))