from django.conf import settings
from django.core.management import BaseCommand
from web3 import Web3, HTTPProvider

try:    
    from poc_webapp.settings import ETHEREUM_NODE_URI
except:
    from poc_webapp.poc_webapp.settings import ETHEREUM_NODE_URI

w3 = Web3(HTTPProvider(ETHEREUM_NODE_URI))

with open("/app/contracts/user_sol_Login.abi") as f:
    abi = f.read()
with open("/app/contracts/user_sol_Login.bin") as f:
    contract_bytecode = f.read()

server_address = w3.eth.accounts[0]

class Command(BaseCommand):
    help = 'Creates contract on the "ETHEREUM_NODE_URI\
            and prints address of this contract'
    def handle(self, *args, **options):

        contract = w3.eth.contract(abi = abi, bytecode = contract_bytecode)

        tx_hash = contract.constructor().transact(transaction = {"from": server_address})

        print(f'Received txn_hash={tx_hash} ...')
        print('Waiting for transaction receipt...')
        txn_receipt = w3.eth.waitForTransactionReceipt(tx_hash)
        print(f'Received transaction receipt: {txn_receipt}')

        contract_address = txn_receipt['contractAddress']
        with open("contracts/contract_address.txt", 'w+') as f:
            f.write(contract_address)
        print('contract_address:', contract_address, 'written to file')
        
