import json

from web3 import Web3
from django.core.management import BaseCommand
from .create_contract import abi, contract_bytecode
from django_ethereum_events.chainevents import AbstractEventReceiver
from django_ethereum_events.models import MonitoredEvent
from django_ethereum_events.utils import HexJsonEncoder


from ...models import BlockChainUser

# from poc_webapp.settings import ETHEREUM_NODE_URI
from django.conf import settings
from django_ethereum_events.tasks import event_listener

with open("/app/contracts/contract_address.txt") as f:
    contract_address = f.read()

class CustomReceiver(AbstractEventReceiver):
    def save(self, decoded_event):
        """Saves decoded received event"""

        data = decoded_event["args"]
        client_address = data['_sender']
        challenge_from_contract = data['_challenge']
        print("-----------------------------")
        print(f"CLIENT_ADDRESS | CHALLENGE : {client_address} | {challenge_from_contract}")
        print("-----------------------------")
        user_q_set = BlockChainUser.objects.filter(user__username=client_address)
        if len(user_q_set) == 1:
            # that should be always the case
            blockchain_user = user_q_set[0]
            if blockchain_user.challenge == challenge_from_contract:
                # everything is fine
                blockchain_user.is_authenticated = True
                blockchain_user.save()


receiver = "blockchain_auth.management.commands.register_events.CustomReceiver"

# List of ethereum events to monitor the blockchain for
DEFAULT_EVENTS = [
    ("LoginAttempt", contract_address, abi, receiver),
]


class Command(BaseCommand):
    help = "Register event with given contract_address, abi and receiver \
        defined in DEFAULT_EVENTS. Should be started once, after contract deployment"

    def handle(self, *args, **options):
        monitored_events = MonitoredEvent.objects.all()
        for event in DEFAULT_EVENTS:
            if not monitored_events.filter(
                name=event[0], contract_address__iexact=event[1]
            ).exists():
                self.stdout.write(
                    f"Creating monitor for event {event[0]} at {event[1]}"
                )
                MonitoredEvent.objects.register_event(*event)

        self.stdout.write(self.style.SUCCESS("Events are up to date"))
