from django.core.management import BaseCommand
from django_ethereum_events.models import Daemon, MonitoredEvent, FailedEventLog

class Command(BaseCommand):
    help = 'For debbuging, prints registreted events'

    def handle(self, *args, **options):
        print('monitored events: ', MonitoredEvent.objects.all())
