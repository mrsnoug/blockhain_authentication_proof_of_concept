function init() {
    //new way of init web3; metamask integration from 16th Nov
    //https://docs.metamask.io/guide/provider-migration.html#table-of-contents
    const ethEnabled = () => {
        if (window.ethereum) {
            window.web3 = new Web3(window.ethereum);
            window.ethereum.enable();
            return true;
        }
        return false;
    }
    if (!ethEnabled()) {
        alert("Please install MetaMask to use this dApp!");
    }
    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
        console.log(web3);
        web3.eth.getAccounts((err, accounts) => {
            // Check for wallet being locked
            if (err) {
                throw err;
            }
            if (accounts.length == 0) {
                $('[data-web3auth-display').hide();
                $('[data-web3auth-display="wallet-locked"]').show();
            } else {
                $('[data-web3auth-display').hide();
                $('[data-web3auth-display="wallet-available"]').show();
                console.log(accounts[0]);
                document.getElementById('id_ethereum_address').value = accounts[0];
            }
        });
    } else {
        $('[data-web3auth-display').hide();
        $('[data-web3auth-display="wallet-available"]').show();
    }
}

//this shouldn't be like that but it's JS and I'm not JS dev although it works
function login(abi, challenge, smart_contract_address) {
    const ethEnabled = () => {
        if (window.ethereum) {
            window.web3 = new Web3(window.ethereum);
            window.ethereum.enable();
            return true;
        }
        return false;
    }
    if (!ethEnabled()) {
        alert("Please install MetaMask to use this dApp!");
    }
    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
        console.log(web3);
        web3.eth.getAccounts((err, accounts) => {
            // Check for wallet being locked
            if (err) {
                throw err;
            }
            if (accounts.length == 0) {
                $('[data-web3auth-display').hide();
                $('[data-web3auth-display="wallet-locked"]').show();
            } else {
                $('[data-web3auth-display').hide();
                $('[data-web3auth-display="wallet-available"]').show();
                console.log(accounts[0]);
                var json_abi = JSON.parse(abi);
                let contract = new web3.eth.Contract(json_abi, smart_contract_address);
                contract.methods.login(challenge).send({ from: accounts[0] });


            }
        });
    }
}