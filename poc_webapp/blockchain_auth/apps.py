from django.apps import AppConfig


class BlockchainAuthConfig(AppConfig):
    name = 'blockchain_auth'
