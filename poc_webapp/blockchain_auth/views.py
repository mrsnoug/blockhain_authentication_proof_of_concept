import hashlib
import secrets
import datetime

from django.shortcuts import render
from django.views import View
from django.contrib.auth.models import User

from .forms import CustomLoginForm
from .models import BlockChainUser


class HomeView(View):
    template_name = "blockchain_auth/home.html"

    def get(self, request):
        if token := request.COOKIES.get("blockchain_auth_token"):
            # we have a blockchain_auth_token present, which means that user is either not yet
            # authenticated, authenticated or he/she tries to send expired cookie manually
            blockchain_user_qset = BlockChainUser.objects.filter(auth_token=token)
            if len(blockchain_user_qset) == 1:
                # great! We have that user in database, so token is not fake
                blockchain_user = blockchain_user_qset[0]
                if (
                    blockchain_user.auth_token_expiration
                    - datetime.datetime.now(
                        blockchain_user.auth_token_expiration.tzinfo
                    )
                ).total_seconds() > 0:
                    # Token did not expire
                    if blockchain_user.is_authenticated:
                        # This means that user is authenticated, considering user as logged in!
                        return render(
                            request=request,
                            template_name=self.template_name,
                            context={"eth_address": blockchain_user.user.username},
                        )
                else:
                    # User token has expired cookie so we set his is_authenticated to False,
                    # just to have everything clear
                    blockchain_user.is_authenticated = False

        return render(request=request, template_name=self.template_name)


class LoginView(View):
    template_name = "blockchain_auth/login.html"
    create_transaction_template = "blockchain_auth/transact.html"
    form = CustomLoginForm()

    with open("/app/contracts/user_sol_Login.abi") as f:
        abi = f.read()

    def _getContractAddress(self):
        "Helper function to deal with creation of the file"
        try:
            f = open("/app/contracts/contract_address.txt")
            return f.read()
        except FileNotFoundError:
            f = open("/app/contracts/contract_address.txt", "w")
            return f.read()

    def get(self, request):
        return render(
            request=request,
            template_name=self.template_name,
            context={"form": self.form},
        )

    def post(self, request):
        uploaded_form = CustomLoginForm(request.POST)
        if uploaded_form.is_valid():
            ethereum_address = uploaded_form.cleaned_data.get("ethereum_address")

            auth_token_cookie = hashlib.sha256(
                bytes(str(secrets.randbits(512)), "utf-8")
            ).hexdigest()

            challenge = hashlib.sha256(bytes(auth_token_cookie, "utf-8")).hexdigest()

            context = {
                "challenge": challenge,
                "contract_address": self._getContractAddress(),
                "abi": self.abi,
            }

            response = render(
                request=request,
                template_name=self.create_transaction_template,
                context=context,
            )
            expiration = datetime.datetime.utcnow() + datetime.timedelta(days=1)

            user_tuple = User.objects.get_or_create(username=ethereum_address)
            if user_tuple[1]:
                # This user was just created, so he/she is visiting for the first time
                user = user_tuple[0]
                user.set_unusable_password()
                user.save()
                blockchain_user_tuple = BlockChainUser.objects.get_or_create(user=user)
                if blockchain_user_tuple[1]:
                    # Since Django user was just created then blockchain user had to be created
                    # as well. If that is not the case then something went terribly wrong
                    blockchain_user = blockchain_user_tuple[0]
                    blockchain_user.is_authenticated = False
                    blockchain_user.challenge = challenge
                    blockchain_user.auth_token = auth_token_cookie
                    blockchain_user.auth_token_expiration = expiration
                    blockchain_user.save()
            else:
                # user was already in a database
                user = user_tuple[0]
                blockchain_user_tuple = BlockChainUser.objects.get_or_create(user=user)
                blockchain_user = blockchain_user_tuple[0]
                if (
                    blockchain_user.auth_token_expiration
                    - datetime.datetime.now(
                        blockchain_user.auth_token_expiration.tzinfo
                    )
                ).total_seconds() <= 0:
                    # User exists, but his token has already expired so we give him a new one
                    # and we treat him pretty much as a "first timer"
                    blockchain_user.is_authenticated = False
                    blockchain_user.challenge = challenge
                    blockchain_user.auth_token = auth_token_cookie
                    blockchain_user.auth_token_expiration = expiration
                    blockchain_user.save()
                else:
                    # user is still authenticated so we can give him his old cookie
                    # this token is probably already there, but just to make sure
                    response.set_cookie(
                        key="blockchain_auth_token",
                        value=blockchain_user.auth_token,
                        httponly=True,
                        expires=blockchain_user.auth_token_expiration,
                    )
                    return response

            response.set_cookie(
                key="blockchain_auth_token",
                value=auth_token_cookie,
                httponly=True,
                expires=expiration,
            )
            return response
        else:
            # calling get from post is not a proper design pattern... but this is just a Proof
            # of Concept project, so it will work
            return self.get(request=request)
