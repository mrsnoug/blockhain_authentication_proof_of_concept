from django.urls import path
from django.conf.urls.static import static
from . import views
from django.conf import settings

urlpatterns = [
    path("", views.HomeView.as_view()),
    path("login/", views.LoginView.as_view()),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
