from django import forms


class CustomLoginForm(forms.Form):
    ethereum_address = forms.CharField(required=False)
