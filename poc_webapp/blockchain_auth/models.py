from django.db import models
from django.contrib.auth.models import User as UserModel


class BlockChainUser(models.Model):
    # basically username of that user will be his/her Ethereum address
    user = models.OneToOneField(UserModel, models.CASCADE, primary_key=True)

    # generated cookie token to make sure no one is impersonating that user
    auth_token = models.TextField(null=True)
    auth_token_expiration = models.DateTimeField(null=True)

    # challenge generated by backend
    challenge = models.TextField(null=True)
    is_authenticated = models.BooleanField(default=False)
