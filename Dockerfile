FROM python:3.8
COPY ./config/requirements.txt /app/requirements.txt 
WORKDIR /app
RUN pip install -r requirements.txt
