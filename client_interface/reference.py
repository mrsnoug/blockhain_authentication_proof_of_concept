from web3 import Web3


with open("contracts/user_sol_Login.abi", "r") as f:
    abi = f.read()

with open("contracts/user_sol_Login.bin", "r") as f:
    bytecode = f.read()

w3 = Web3(Web3.HTTPProvider("http://127.0.0.1:8545"))

contract = w3.eth.contract(abi=abi, bytecode=bytecode)

# creating a transaction, i.e.: publishing contract on the blockchain using first Ethereum
# account generated by the ganache
tx_hash = contract.constructor().transact(transaction={"from": w3.eth.accounts[0]})

# getting a "receipt" of above transaction, I think that will be "server side"
tx_recepit = w3.eth.waitForTransactionReceipt(tx_hash)

# "client side". Lines below should emit an event which then server should be able to "read"
client = w3.eth.contract(address=tx_recepit.contractAddress, abi=abi)
client.functions.login("SERVER_CHALLENGE_HERE").call()
