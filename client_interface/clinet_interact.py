from web3 import Web3
from time import sleep
# from poc_webapp.poc_webapp.settings import ETHEREUM_NODE_URI
ETHEREUM_NODE_URI = 'http://localhost:8545'

"""Client side template interface for contract function execution in Python
    Will be implemented in JS with metamask"""

#prepare env; those will be delivered with JS implementation
w3 = Web3(Web3.HTTPProvider(ETHEREUM_NODE_URI))
with open("contracts/user_sol_Login.abi", "r") as f:
    abi = f.read()          # in JS this will be delivered
with open("contracts/user_sol_Login.bin", "r") as f:
    bytecode = f.read()     # in JS this will be delivered
with open("contracts/contract_address.txt", "r") as f:
    contract_address = f.read()        # in JS this will be delivered
server_challange = '1fa5b7d183eea40cf172d500bb9c94ecccc48b72ff8c4a86159a8f8f72817b05'    # in JS this will be delivered

#will be replaced with metamask implementation
private_key = '0xe56fe68b66f1e669497a7cf4e18c09d9843241535c2e0d7a4d87829d0b288d03'

print('w3 is connected: ', w3.isConnected())
if __name__ == "__main__" and w3.isConnected() and contract_address:

    #private_key for testing is hard codded, will be changed with metamask implementation
    account = w3.eth.account.from_key(private_key)
    print('address used:', account.address)
    
    contract = w3.eth.contract(address = contract_address, abi = abi)
    while True:
        transaction = contract.functions.login(server_challange)\
            .transact(transaction={"from": account.address})
        print('transaction receipt: ', transaction)
        sleep(5)

else:
    print('something went wrong')