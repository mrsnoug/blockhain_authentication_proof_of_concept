## How to run

#### requirements:
* Docker and Docker Compose installed
* Docker daemon up and running
* Metamask web-browser extension

1. ##### running application (assuming you're running linux):

```
git clone https://gitlab.com/mrsnoug/blockhain_authentication_proof_of_concept.git && 
cd blockhain_authentication_proof_of_concept &&
docker-compose up
```


2. ##### when running on ganache (default):
* copy one of the ganache's wallets private keys (should be visible in docker-compose logs)
* import wallet to Metamask extension
* connect Metamask to localhost:8545
* after clicking login button on the web-app, submit transaction in Metamask
* you should be authenticated to the application

## Architecture

![architecture](blockchain_auth_architecture.png)